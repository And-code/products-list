@extends('layout')

@section('content')

    <p>Product form</p>

    <div class="row">

        <form action="{{ $route ?? route('products.store') }}" method="post">
            @csrf
            @if(!empty($method))
                <input name="_method" type="hidden" value="{{ $method }}">
            @endif

            @error('name')
            <hr>
            <div class="alert alert-danger"> {{ $message }} </div>
            @enderror
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name"
                       name="name" placeholder="Name"
                       value="{{ old('name') ?? $product->name ?? '' }}">
            </div>

            @error('price')
            <hr>
            <div class="alert alert-danger"> {{ $message }} </div>
            @enderror
            <div class="mb-3">
                <label for="price" class="form-label">Price</label>
                <input type="text" class="form-control" id="price" name="price" placeholder="price"
                       value="{{ old('price') ?? $product->price ?? '' }}">
            </div>

            @error('description')
            <hr>
            <div class="alert alert-danger"> {{ $message }} </div>
            @enderror
            <div class="mb-3">
                <label for="description" class="form-label">Description</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{ old('description')  ?? $product->description ?? ''}}</textarea>
            </div>

            <button type="submit" class="btn btn-success">{{ $button ?? 'Создать'}}</button>
        </form>
    </div>


@endsection
