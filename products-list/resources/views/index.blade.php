@extends('layout')

@section('content')

    @include('products', ['products' => $products, 'title' => $title, 'edit' => $edit ?? null ])

@endsection
