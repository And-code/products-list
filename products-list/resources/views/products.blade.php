<div class="row">

    <h2>{{ $title }}</h2>

    @isset($edit)
        <div class="col mb-3">
            <a href="{{ route('products.create') }}" class="btn btn-success">Создать новый</a>
        </div>

    @endisset

    <div class="row mb-3">
        {{ $products->links() }}
    </div>

    <table class="table table-striped">
        <thead class="thead-light">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>
            <th scope="col">Date</th>
            @isset($edit)
            <th scope="col">Edit</th>
            @endisset
        </tr>
        </thead>
        <tbody>
        @forelse ($products as $key => $product)
            <tr>
                <th scope="row">{{ $product->id }}</th>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->updated_at }}</td>
                @isset($edit)
                <th scope="col">
                    <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-success">Редактировать</a>
                    <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </form>
                </th>
                @endisset
            </tr>
        @empty
            <tr>
                <td colspan="6">No products</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    <div class="row mb-3">
        {{ $products->links() }}
    </div>
</div>
