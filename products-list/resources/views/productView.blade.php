@extends('layout')

@section('content')

    <p>Product details</p>

    <div class="row">

            <div class="col">
                <div class="card">
                    <div class="card-body">

                            <h5 class="card-title">Product id: {{ $product->id }}</h5>
                            <h5 class="card-text">Product name: {{ $product->name }}</h5>
                            <h5 class="card-text">Description: {{ $product->description }}</h5>
                            <h5 class="card-text">Price: {{ $product->price }}</h5>

                        <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-success">Редактировать</a>
                        <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form>

                    </div>
                </div>
            </div>

    </div>


@endsection
