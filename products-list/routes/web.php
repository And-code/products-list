<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return redirect('products');
});

Route::get('/products', [HomeController::class, 'index'])->name('products');
Route::get('/product-list', [HomeController::class, 'edit'])->name('productsEdit');

// product's CRUD
Route::resource('products', ProductController::class)
    ->except(['index']);
