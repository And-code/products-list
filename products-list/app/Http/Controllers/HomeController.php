<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('index', [
            'products' => Product::orderBy('updated_at', 'desc')->paginate(10),
            'title' => 'Products',
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return response()->view('index', [
            'products' => Product::orderBy('updated_at', 'desc')->paginate(10),
            'title' => 'Products edit',
            'edit' => true,
        ]);
    }
}
