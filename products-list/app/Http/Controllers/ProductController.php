<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('productForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'price' => 'required|numeric|between:0.00,99999999.99|regex:/^\d+(\.\d{1,2})?$/',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('products.create')
                ->withErrors($validator)
                ->withInput();
        }

        $product = new Product([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
        ]);
        $product->save();

        return redirect()->route('productsEdit')->with([
            'flash' => 'Product created!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Product $product)
    {
        return view('productForm', [
            'product' => $product,
            'method' => 'PUT',
            'button' => 'Обновить',
            'route' => route('products.update', ['product' => $product->id]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'price' => 'required|numeric|between:0.00,99999999.99|regex:/^\d+(\.\d{1,2})?$/',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('products.edit', ['product' => $product->id])
                ->withErrors($validator)
                ->withInput();
        }


        $product->fill([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
        ]);

        $product->save();

        return redirect()->route('productsEdit')->with([
            'flash' => 'Product updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('productsEdit')->with([
            'flash' => "Product with id={$product->id} deleted!"
        ]);
    }
}
