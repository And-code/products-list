Тестовое приложение с реализацией CRUD для товаров. Без авторизации и регистрации.

- страница http://localhost/products - выводит все добавленные товары
- страница http://localhost/product-list - выводит все добавленные товары с CRUD

Шаги запуска:

git clone https://gitlab.com/And-code/products-list.git

cd products-list

cp .env.example .env

docker-compose up -d --build

docker-compose exec app bash

composer install

php artisan key:generate

php artisan migrate --seed

запуск в браузере: http://0.0.0.0:8001
